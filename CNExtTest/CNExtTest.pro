QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_envtest.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CNExt/release/ -lCNExt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CNExt/debug/ -lCNExt

INCLUDEPATH += $$PWD/..
DEPENDPATH += $$PWD/..

win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
LIBS += -lnotes
