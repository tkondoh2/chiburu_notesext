﻿#include <QtTest>

// add necessary includes here
#include <CNExt/api.h>
#include <CNExt/environment.h>
#include <CNExt/lmbcslist.h>
#include <QTextStream>

class EnvTest : public QObject
{
  Q_OBJECT

public:
  EnvTest();
  ~EnvTest();

private slots:
  void initTestCase();
  void cleanupTestCase();
  void test_envReadWrite();
  void test_QStringTranslate();
  void test_envLong();
  void test_envInt();
  void test_lmbcsListAppend();
  void test_lmbcsListAt();
  void test_lmbcsListRemove();
  void test_lmbcsListRemoveOne();
  void test_lmbcsListRemoveAll();
  void test_lmbcsListClear();
  void test_lmbcsListCopy();
  void test_lmbcsListSplit();
  void test_lmbcsListContains();
  void test_lmbcsListJoin();

private:
  QTextStream str_;
};

EnvTest::EnvTest()
  : str_(stdout)
{
}

EnvTest::~EnvTest()
{
}

void EnvTest::initTestCase()
{
}

void EnvTest::cleanupTestCase()
{
//  CNExt::SetEnvironment()(lmbcs("TestName"), CNExt::Lmbcs());
//  CNExt::SetEnvironment()(lmbcs("TestName2"), CNExt::Lmbcs());
//  CNExt::SetEnvironment()(lmbcs("TestName3"), CNExt::Lmbcs());
//  CNExt::SetEnvironment()(lmbcs("TestName4"), CNExt::Lmbcs());
}

void EnvTest::test_envReadWrite()
{
  CNExt::SetEnvironment()("TestName", "TestValue");
  CNExt::Lmbcs text = CNExt::GetEnvironment()("TestName");
  QVERIFY2(text == "TestValue", "Failure");
}

void EnvTest::test_QStringTranslate()
{
  QString v = QString::fromLocal8Bit("テストの値2");
  CNExt::SetEnvironment()("TestName2", lmbcs(v));
  CNExt::Lmbcs text = CNExt::GetEnvironment()("TestName2");
  QVERIFY2(text == lmbcs(v), "Failure");
}

void EnvTest::test_envLong()
{
  long v = 12345;
  CNExt::SetEnvironmentInt()("TestName3", static_cast<int>(v));
  long x = CNExt::GetEnvironmentLong()("TestName3");
  QVERIFY2(v == x, "Failure");
//  str_ << "long size = " << sizeof(long) << endl;
}

void EnvTest::test_envInt()
{
  int v = 123;
  CNExt::SetEnvironmentInt()("TestName4", v);
  int x = CNExt::GetEnvironmentInt()("TestName4");
  QVERIFY2(v == x, "Failure");
//  str_ << "int size = " << sizeof(int) << endl;
}

void EnvTest::test_lmbcsListAppend()
{
  CNExt::LmbcsList list;

  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));

  WORD num = list.append(value1);
  QVERIFY2(num == 1, "Failure");

  num = list.append(value2);
  QVERIFY2(num == 2, "Failure");
}

void EnvTest::test_lmbcsListAt()
{
  CNExt::LmbcsList list;
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  list.append(value1);
  list.append(value2);

  CNExt::SetEnvironment()("TestName5", list.at(0));
  CNExt::Lmbcs getValue = CNExt::GetEnvironment()("TestName5");
  QVERIFY2(getValue == value1, "Failure");

  CNExt::SetEnvironment()("TestName6", list.at(1));
  getValue = CNExt::GetEnvironment()("TestName6");
  QVERIFY2(getValue == value2, "Failure");
}

void EnvTest::test_lmbcsListRemove()
{
  CNExt::LmbcsList list;
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム3"));
  CNExt::Lmbcs value4 = lmbcs(QString::fromLocal8Bit("アイテム4"));

  list.append(value1);
  list.append(value2);
  list.append(value3);
  list.append(value4);
  QVERIFY2(list.numEntries() == 4, "Failure");

  list.removeAt(1);
  QVERIFY2(list.numEntries() == 3, "Failure");

  CNExt::Lmbcs getValue = list.at(1);
  QVERIFY2(getValue == value3, "Failure");

  list.removeFirst();
  QVERIFY2(list.numEntries() == 2, "Failure");

  getValue = list.first();
  QVERIFY2(getValue == value3, "Failure");

  list.removeLast();
  QVERIFY2(list.numEntries() == 1, "Failure");

  getValue = list.last();
  QVERIFY2(getValue == value3, "Failure");

  list.removeLast();
  QVERIFY2(list.isEmpty(), "Failure");
}

void EnvTest::test_lmbcsListRemoveOne()
{
  CNExt::LmbcsList list;
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム3"));
  CNExt::Lmbcs value4 = lmbcs(QString::fromLocal8Bit("アイテム4"));

  list.append(value1);
  list.append(value2);
  list.append(value3);
  list.append(value2);
  QVERIFY2(list.numEntries() == 4, "Failure");

  QVERIFY2(!list.removeOne(value4), "Failure");
  QVERIFY2(list.removeOne(value2), "Failure");
  QVERIFY2(list.numEntries() == 3, "Failure");
  QVERIFY2(list.at(0) == value1, "Failure");
  QVERIFY2(list.at(1) == value3, "Failure");
  QVERIFY2(list.at(2) == value2, "Failure");
}

void EnvTest::test_lmbcsListRemoveAll()
{
  CNExt::LmbcsList list;
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム3"));

  list.append(value2);
  list.append(value1);
  list.append(value2);
  list.append(value3);
  QVERIFY2(list.numEntries() == 4, "Failure");

  QVERIFY2(list.removeAll(value2) == 2, "Failure");
  QVERIFY2(list.numEntries() == 2, "Failure");
  QVERIFY2(list.at(0) == value1, "Failure");
  QVERIFY2(list.at(1) == value3, "Failure");
}

void EnvTest::test_lmbcsListClear()
{
  CNExt::LmbcsList list;
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム3"));

  list.append(value2);
  list.append(value1);
  list.append(value2);
  list.append(value3);
  QVERIFY2(list.numEntries() == 4, "Failure");
  list.clear();
  QVERIFY2(list.numEntries() == 0, "Failure");
}

void EnvTest::test_lmbcsListCopy()
{
  CNExt::LmbcsList list;
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム3"));

  list.append(value2);
  list.append(value1);
  list.append(value2);
  list.append(value3);
  QVERIFY2(list.numEntries() == 4, "Failure");

  CNExt::LmbcsList list2(list);
  QVERIFY2(list2.numEntries() == 4, "Failure");

  QVERIFY2(list2.at(2) == list.at(0), "Failure");
  QVERIFY2(list2.at(1) == list.at(1), "Failure");
  QVERIFY2(list2.at(0) == list.at(2), "Failure");
  QVERIFY2(list2.at(3) == list.at(3), "Failure");

  CNExt::LmbcsList list3;
  list3.append(value1);
  list3.append(value2);
  list3.append(value3);
  QVERIFY2(list3.numEntries() == 3, "Failure");

  list3 = list2;
  QVERIFY2(list3.numEntries() == 4, "Failure");
  QVERIFY2(list3.at(2) == list2.at(0), "Failure");
  QVERIFY2(list3.at(1) == list2.at(1), "Failure");
  QVERIFY2(list3.at(0) == list2.at(2), "Failure");
  QVERIFY2(list3.at(3) == list2.at(3), "Failure");
}

void EnvTest::test_lmbcsListContains()
{
  CNExt::LmbcsList list;
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム3"));
  CNExt::Lmbcs value4 = lmbcs(QString::fromLocal8Bit("アイテム4"));

  list.append(value1);
  list.append(value2);
  list.append(value3);
  QVERIFY2(list.numEntries() == 3, "Failure");

  QVERIFY2(list.contains(value2), "Failure");
  QVERIFY2(!list.contains(value4), "Failure");
}

void EnvTest::test_lmbcsListSplit()
{
  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム2"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム3"));

  CNExt::Lmbcs joined = value1 + "," + value2 + "," + value3;

  CNExt::LmbcsList list = joined.split(",");
  QVERIFY2(list.numEntries() == 3, "Failure");

  QVERIFY2(list.at(0) == value1, "Failure");
  QVERIFY2(list.at(1) == value2, "Failure");
  QVERIFY2(list.at(2) == value3, "Failure");
}

void EnvTest::test_lmbcsListJoin()
{
  CNExt::LmbcsList list;

  CNExt::Lmbcs value1 = lmbcs(QString::fromLocal8Bit("アイテム1"));
  CNExt::Lmbcs value2 = lmbcs(QString::fromLocal8Bit("アイテム22"));
  CNExt::Lmbcs value3 = lmbcs(QString::fromLocal8Bit("アイテム333"));

  list << value1 << value2 << value3;
  CNExt::Lmbcs joined = list.join("; ");
  QVERIFY2(joined.length() == (5 + 2 + 6 + 2 + 7), "Failure");
  QVERIFY2(joined.size() == (13 + 2 + 14 + 2 + 15), "Failure");
  QVERIFY2(joined == lmbcs(QString::fromLocal8Bit("アイテム1; アイテム22; アイテム333")), "Failure");
}

int main(int argc, char *argv[])
{
  CNExt::Api api(argc, argv);
  EnvTest tc;
  QTEST_SET_MAIN_SOURCE_PATH
  return QTest::qExec(&tc, argc, argv);
}

#include "tst_envtest.moc"
