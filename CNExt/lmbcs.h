﻿#ifndef CNEXT_LMBCS_H
#define CNEXT_LMBCS_H

#include <CNExt/result.h>
#include <QByteArray>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nls.h>
#include <osmisc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace CNExt {

class LmbcsList;

/**
 * @brief NLS_STATUS値を返す関数用ラップクラス
 * @class NlsResultful
 */
class CNEXTSHARED_EXPORT NlsResultful
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  NlsResultful();

  /**
   * @brief 最終実行結果を返す。
   * @return 最終実行結果
   */
  NLS_STATUS lastResult() const;

  /**
   * @brief エラーがあれば真を返す。
   * @return エラーがあれば真
   */
  bool hasError() const;

  /**
   * @brief エラーがなければ真を返す。
   * @return エラーがなければ真
   */
  bool noError() const;

  /**
   * @brief (静的メソッドなどで)外部から実行結果を設定する。
   * @param status 実行結果
   */
  void setLastResult(NLS_STATUS status);

protected:
  /**
   * @brief NLS関数を実行結果を設定する。
   * @param result NLS関数を実行結果
   */
  void exec(NLS_STATUS result) const;

private:
  mutable NLS_STATUS lastResult_;
};


/**
 * @brief LMBCS文字列クラス
 * @class Lmbcs
 */
class CNEXTSHARED_EXPORT Lmbcs
    : public NlsResultful
{
public:
  /**
   * @brief NLS_translateで使用するバッファの最大サイズ
   */
  static const WORD MAX_NLS_BUFSIZE = 0xfffe;

  /**
   * @brief Unicode文字のサイズ(バイト単位)
   */
  static const std::size_t UTF16_SIZE = sizeof(ushort);
  /**
   * @brief Unicode文字列の最大文字数(文字単位)
   */
  static const WORD UTF16_MAX_LEN = MAX_NLS_BUFSIZE / UTF16_SIZE;

  /**
   * @brief Unicode文字列の最大サイズ(バイト単位)
   */
  static const WORD UTF16_MAX_SIZE = UTF16_MAX_LEN * UTF16_SIZE;

  /**
   * @brief デフォルトコンストラクタ
   */
  Lmbcs();

  /**
   * @brief ヌル終端LMBCS文字列で初期化する。
   * @param c_str ヌル終端LMBCS文字列
   */
  Lmbcs(const char *c_str);

  /**
   * @brief サイズ指定LMBCS文字列で初期化する。
   * @param ptr LMBCS文字列
   * @param len LMBCS文字列のサイズ
   */
  Lmbcs(const char *ptr, WORD len);

  /**
   * @brief LMBCS文字列への不変ポインタを返す。
   * @return LMBCS文字列への不変ポインタ
   */
  const char *constData() const;

  /**
   * @brief QStringオブジェクトに変換して返す。
   * @return 変換したQStringオブジェクト
   */
  QString toQString() const;

  /**
   * @brief QStringオブジェクトからLmbcs文字列に変換して返す。
   * @param from 変換元QStringオブジェクト
   * @return 変換したLmbcsオブジェクト
   */
  static Lmbcs fromQString(const QString &from);

  /**
   * @brief LMBCS文字列のバイトサイズを返す。
   * @return LMBCS文字列のバイトサイズ
   */
  WORD size() const;

  /**
   * @brief LMBCS文字列の文字数を返す。
   * @return LMBCS文字列の文字数
   */
  WORD length() const;

  /**
   * @brief LMBCS文字列が空なら真を返す。
   * @return LMBCS文字列が空なら真
   */
  bool isEmpty() const;

  LmbcsList split(const Lmbcs &delimiter) const;

  /**
   * @brief 文字セット文字列の文字数を返す。
   * @param c_str 文字セット文字列
   * @param byteSize 文字セット文字列のバイト長
   * @param pInfo 文字セット(デフォルトはLMBCS)
   * @return 文字数
   */
  static WORD getCharSize(
      const char* c_str
      , WORD byteSize
      , NLS_PINFO pInfo = OSGetLMBCSCLS()
      );

  /**
   * @brief 文字セット文字列の文字数に対応するバイト長を返す。
   * @param c_str 文字セット文字列
   * @param len 文字セット文字列の文字数
   * @param pInfo 文字セット(デフォルトはLMBCS)
   * @return バイト長
   */
  static WORD getByteSize(
      const char* c_str
      , WORD len
      , NLS_PINFO pInfo = OSGetLMBCSCLS()
      );

  /**
   * @brief 等号演算子
   * @param lhs 左辺のLmbcs
   * @param rhs 右辺のLmbcs
   * @return 等価なら真
   */
  friend CNEXTSHARED_EXPORT bool operator ==(
      const Lmbcs &lhs
      , const Lmbcs &rhs
      );

  Lmbcs &append(const Lmbcs &other);

  /**
   * @brief 加算代入演算子
   * @param other つなげるTextオブジェクト
   * @return otherをつなげた後の自身への参照
   */
  Lmbcs &operator +=(const Lmbcs &other);

  /**
   * @brief 加算演算子
   * @param lhs 左辺のText
   * @param rhs 右辺のText
   * @return 左辺と右辺をつなげた新しいTextオブジェクト
   */
  friend CNEXTSHARED_EXPORT Lmbcs operator +(
      const Lmbcs& lhs
      , const Lmbcs& rhs
      );

private:
  QByteArray value_;
};


/**
 * @brief NLSキャラクタセットのロード、アンロードを自動化するクラス
 * @class NLSCharSet
 */
class NLSCharSet
    : public NlsResultful
{
public:
  /**
   * @brief コンストラクタ
   * @param CSID キャラクタセットID
   */
  NLSCharSet(WORD CSID);

  /**
   * @brief デストラクタ
   */
  virtual ~NLSCharSet();

  /**
   * @brief NLS_PINFO変換演算子
   */
  operator NLS_PINFO();

private:
  NLS_PINFO info_;
};


inline NLSCharSet::NLSCharSet(WORD CSID)
  : NlsResultful()
{
  exec(NLS_load_charset(CSID, &info_));
}

inline  NLSCharSet::~NLSCharSet()
{
  NLS_unload_charset(info_);
}

inline NLSCharSet::operator NLS_PINFO()
{
  return info_;
}


inline NlsResultful::NlsResultful()
  : lastResult_(NLS_SUCCESS)
{
}

inline bool NlsResultful::hasError() const
{
  return (lastResult_ != NLS_SUCCESS);
}

inline bool NlsResultful::noError() const
{
  return !hasError();
}

inline void NlsResultful::setLastResult(NLS_STATUS status)
{
  lastResult_ = status;
}

inline void NlsResultful::exec(NLS_STATUS result) const
{
  lastResult_ = result;
}


inline Lmbcs::Lmbcs(const char* c_str)
  : value_(c_str)
{
}

inline Lmbcs::Lmbcs(const char* ptr, WORD len)
  : value_(ptr, static_cast<int>(len))
{
}

inline const char *Lmbcs::constData() const
{
  return value_.constData();
}

inline bool Lmbcs::isEmpty() const
{
  return value_.isEmpty();
}

inline bool operator ==(const Lmbcs &lhs, const Lmbcs &rhs)
{
  return (lhs.value_ == rhs.value_);
}

inline Lmbcs& Lmbcs::operator +=(const Lmbcs& other)
{
  return append(other);
}

inline Lmbcs operator +(const Lmbcs& lhs, const Lmbcs& rhs)
{
  Lmbcs ret = lhs;
  ret += rhs;
  return ret;
}

} // namespace CNExt

/**
 * @brief QStringからLmbcsを作成するコンビニエンス関数
 * @param from QStringオブジェクト
 * @return Lmbcsオブジェクト
 */
inline CNExt::Lmbcs lmbcs(const QString &from)
{
  return CNExt::Lmbcs::fromQString(from);
}

#endif // CNEXT_LMBCS_H
