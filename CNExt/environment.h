﻿#ifndef CNEXT_ENVIRONMENT_H
#define CNEXT_ENVIRONMENT_H

#include <CNExt/lmbcs.h>

namespace CNExt {

/**
 * @brief 環境変数値(文字列)の取得ファンクタ
 * @class GetEnvironment
 */
class CNEXTSHARED_EXPORT GetEnvironment
{
public:
  Lmbcs operator()(const Lmbcs &name) const;
};

/**
 * @brief 環境変数値(LONG数値)の取得ファンクタ
 * @class GetEnvironmentLong
 */
class CNEXTSHARED_EXPORT GetEnvironmentLong
{
public:
  long operator()(const Lmbcs &name) const;
};

/**
 * @brief 環境変数値(INT数値)の取得ファンクタ
 * @class GetEnvironmentInt
 */
class CNEXTSHARED_EXPORT GetEnvironmentInt
{
public:
  int operator()(const Lmbcs &name) const;
};

/**
 * @brief 環境変数値(文字列)の設定ファンクタ
 * @class SetEnvironment
 */
class CNEXTSHARED_EXPORT SetEnvironment
{
public:
  void operator()(const Lmbcs &name, const Lmbcs &value) const;
};

/**
 * @brief 環境変数値(INT数値)の設定ファンクタ
 * @class SetEnvironmentInt
 */
class CNEXTSHARED_EXPORT SetEnvironmentInt
{
public:
  void operator()(const Lmbcs &name, int value) const;
};

} // namespace CNExt

#endif // CNEXT_ENVIRONMENT_H
