﻿#ifndef CNEXT_API_H
#define CNEXT_API_H

#include <CNExt/result.h>

namespace CNExt {

/**
 * @brief Notes API初期化/終了処理クラス
 * @class Api
 */
class CNEXTSHARED_EXPORT Api
    : public Resultful
{
public:
  /**
   * @brief コンストラクタ
   * @param argc main関数第1引数
   * @param argv main関数第2引数
   */
  Api(int argc, char *argv[]);

  /**
   * @brief デストラクタ
   */
  virtual ~Api();
};

} // namespace CNExt

#endif // CNEXT_API_H
