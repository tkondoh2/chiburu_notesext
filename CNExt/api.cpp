﻿#include "api.h"

namespace CNExt {

Api::Api(int argc, char *argv[])
  : Resultful()
{
  exec(NotesInitExtended(argc, argv));
}

Api::~Api()
{
  if (lastResult().noError())
    NotesTerm();
}

} // namespace CNExt
