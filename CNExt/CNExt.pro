#-------------------------------------------------
#
# Project created by QtCreator 2018-06-18T08:22:34
#
#-------------------------------------------------

QT       -= gui

TARGET = CNExt
TEMPLATE = lib

DEFINES += CNEXT_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    api.cpp \
    result.cpp \
    environment.cpp \
    lmbcs.cpp \
    lmbcslist.cpp

HEADERS += \
        cnext_global.h \
    api.h \
    result.h \
    environment.h \
    lmbcs.h \
    lmbcslist.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH -= $$PWD
DEPENDPATH -= $$PWD
INCLUDEPATH += $$PWD/..
DEPENDPATH += $$PWD/..
LIBS += -lnotes

win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
