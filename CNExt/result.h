﻿#ifndef CNEXT_RESULT_H
#define CNEXT_RESULT_H

#include <CNExt/cnext_global.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace CNExt {

/**
 * @brief APIの処理結果クラス
 * @class Result
 */
class CNEXTSHARED_EXPORT Result
{
public:
  /**
   * @brief デフォルトコンストラクタ
   * @param status STATUS値
   */
  Result(STATUS status = NOERROR);

  /**
   * @brief STATUS型変換演算子
   */
  operator STATUS() const;

  /**
   * @brief フラグを除いたエラー値を返す。
   * @return フラグを除いたエラー値
   */
  STATUS error() const;

  /**
   * @brief エラーがあれば真を返す。
   * @return エラーがあれば真
   */
  bool hasError() const;

  /**
   * @brief エラーがなければ真を返す。
   * @return エラーがなければ真
   */
  bool noError() const;

private:
  STATUS status_;
};

/**
 * @brief 最終結果Resultをクラスに埋め込むクラス
 * @class Resultful
 */
class CNEXTSHARED_EXPORT Resultful
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Resultful();

  /**
   * @brief APIの最終結果を返す。
   * @return APIの最終結果
   */
  const Result &lastResult() const;

protected:
  /**
   * @brief Notes APIを実行してSTATUSを受け取る
   * @param result API実行結果
   * @return エラーがなければ真
   */
  bool exec(const Result &result) const;

private:
  mutable Result lastResult_;
};

inline Result::Result(STATUS status)
  : status_(status)
{
}

inline Result::operator STATUS() const
{
  return status_;
}

inline STATUS Result::error() const
{
  return ERR(status_);
}

inline bool Result::hasError() const
{
  return (error() != NOERROR);
}

inline bool Result::noError() const
{
  return !hasError();
}

inline Resultful::Resultful()
  : lastResult_()
{
}

inline bool Resultful::exec(const Result &result) const
{
  lastResult_ = result;
  return result.noError();
}

inline const Result &Resultful::lastResult() const
{
  return lastResult_;
}

} // namespace CNExt

#endif // CNEXT_RESULT_H
