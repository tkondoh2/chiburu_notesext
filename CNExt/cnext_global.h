#ifndef CNEXT_GLOBAL_H
#define CNEXT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CNEXT_LIBRARY)
#  define CNEXTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CNEXTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CNEXT_GLOBAL_H
