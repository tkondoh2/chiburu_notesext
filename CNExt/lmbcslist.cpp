﻿#include "lmbcslist.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <textlist.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace CNExt {

LmbcsList::LmbcsList(bool fPrefixDataType)
  : Resultful()
  , fPrefixDataType_(fPrefixDataType)
  , mem_()
{
  assign(0);
}

LmbcsList::~LmbcsList()
{
}

LmbcsList::LmbcsList(const LmbcsList &other)
  : Resultful()
  , fPrefixDataType_(other.fPrefixDataType_)
  , mem_()
{
  DHANDLE handle = NULLHANDLE;
  if (exec(ListDuplicate(
             reinterpret_cast<LIST*>(const_cast<void*>(other.mem_.ptr()))
             , fPrefixDataType_
             , &handle
             )))
    mem_.assign(handle);
}

LmbcsList &LmbcsList::operator=(const LmbcsList &other)
{
  if (this != &other)
  {
    DHANDLE handle = NULLHANDLE;
    if (exec(ListDuplicate(
               reinterpret_cast<LIST*>(const_cast<void*>(other.mem_.ptr()))
               , fPrefixDataType_
               , &handle
               )))
    {
      mem_.free();
      mem_.assign(handle);
    }
  }
  return *this;
}

bool LmbcsList::contains(const Lmbcs &entry) const
{
  WORD count = numEntries();
  for (WORD i = 0; i < count; ++i)
  {
    if (at(i) == entry)
      return true;
  }
  return false;
}

Lmbcs LmbcsList::join(const Lmbcs &delimiter) const
{
  QStringList qList;
  WORD count = numEntries();
  for (WORD i = 0; i < count; ++i)
  {
    qList << at(i).toQString();
  }
  QString qJoined = qList.join(delimiter.toQString());
  return lmbcs(qJoined);
}

WORD LmbcsList::size() const
{
  return ListGetSize(const_cast<void*>(mem_.ptr()), fPrefixDataType_);
}

WORD LmbcsList::numEntries() const
{
  return ListGetNumEntries(mem_.ptr(), fPrefixDataType_);
}

bool LmbcsList::isEmpty() const
{
  return (numEntries() == 0);
}

Lmbcs LmbcsList::at(WORD index) const
{
  if (isEmpty())
    return Lmbcs();

  index = std::min<WORD>(index, numEntries() - 1);
  char* pText = nullptr;
  WORD len = 0;
  if (!exec(ListGetText(
         mem_.ptr()
         , fPrefixDataType_
         , index
         , &pText
         , &len
         )))
    return Lmbcs();
  return Lmbcs(pText, len);
}

WORD LmbcsList::append(const Lmbcs &entry)
{
  WORD num = numEntries();
  WORD tSize = size();
  if (!exec(ListAddEntry(
         mem_.handle()
         , fPrefixDataType_
         , &tSize
         , num
         , entry.constData()
         , entry.size()
         )))
    return num;
  return numEntries();
}

void LmbcsList::removeAt(WORD index)
{
  if (isEmpty())
    return;

  index = std::min<WORD>(index, numEntries() - 1);
  WORD tSize = size();
  exec(ListRemoveEntry(
        mem_.handle()
        , fPrefixDataType_
        , &tSize
        , index));
}

bool LmbcsList::removeOne(const Lmbcs &text)
{
  WORD max = numEntries();
  for (WORD i = 0; i < max; ++i)
  {
    if (at(i) == text)
    {
      removeAt(i);
      return true;
    }
  }
  return false;
}

WORD LmbcsList::removeAll(const Lmbcs &text)
{
  WORD max = numEntries();
  WORD count = 0;
  for (WORD i = max; i > 0; --i)
  {
    if (at(i - 1) == text)
    {
      removeAt(i - 1);
      ++count;
    }
  }
  return count;
}

void LmbcsList::clear()
{
  WORD tSize = size();
  exec(ListRemoveAllEntries(
         mem_.handle()
         , fPrefixDataType_
         , &tSize
         ));
}

bool LmbcsList::assign(WORD count)
{
  mem_.free();

  DHANDLE handle;
  void* ptr;
  WORD tSize;
  if (exec(ListAllocate(
                count
                , 0
                , fPrefixDataType_
                , &handle
                , &ptr
                , &tSize
                )))
  {
    mem_.assign(handle, ptr);
    mem_.unlock();
    return true;
  }
  return false;
}

LmbcsList LmbcsList::fromQStringList(const QStringList &qlist)
{
  LmbcsList list;
  foreach (QString item, qlist)
  {
    list.append(lmbcs(item));
  }
  return list;
}

} // namespace CNExt
