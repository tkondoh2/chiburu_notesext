﻿#include "environment.h"

#include <QScopedPointer>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osenv.h>
//#include <osfile.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace CNExt {

Lmbcs GetEnvironment::operator()(const Lmbcs &name) const
{
  QScopedPointer<char, QScopedPointerArrayDeleter<char>> buffer(
        new char[MAXENVVALUE + 1]
        );
  if (!OSGetEnvironmentString(name.constData(), buffer.data(), MAXENVVALUE))
    return Lmbcs();

  return Lmbcs(buffer.data());
}

long GetEnvironmentLong::operator()(const Lmbcs &name) const
{
  return OSGetEnvironmentLong(name.constData());
}

int GetEnvironmentInt::operator()(const Lmbcs &name) const
{
  return OSGetEnvironmentInt(name.constData());
}

void SetEnvironment::operator()(const Lmbcs &name, const Lmbcs &value) const
{
  OSSetEnvironmentVariable(name.constData(), value.constData());
}

void SetEnvironmentInt::operator()(const Lmbcs &name, int value) const
{
  OSSetEnvironmentInt(name.constData(), value);
}

} // namespace CNExt
