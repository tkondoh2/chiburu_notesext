﻿#include "lmbcs.h"

#include "lmbcslist.h"

#include <QString>
#include <QStringList>
#include <QScopedPointer>

namespace CNExt
{

/**
 * @brief 文字列配列用スコープドスマートポインタ
 */
using CharArrayPtr = QScopedPointer<char, QScopedPointerArrayDeleter<char>>;

Lmbcs::Lmbcs()
  : NlsResultful()
{
}

WORD Lmbcs::size() const
{
  return static_cast<WORD>(value_.size());
}

WORD Lmbcs::length() const
{
  return getCharSize(constData(), size());
}

QString Lmbcs::toQString() const
{
  // UNICODE文字コードセットをロード
  NLSCharSet unicodeInfo(NLS_CS_UNICODE);

  // 変換後のUNICODE文字列格納用バッファ
  CharArrayPtr buffer(new char[UTF16_MAX_SIZE]);
  WORD retByteSize = UTF16_MAX_SIZE;

  exec(NLS_translate(
        reinterpret_cast<BYTE*>(const_cast<char*>(constData()))
        , size()
        , reinterpret_cast<BYTE*>(buffer.data())
        , &retByteSize
        , NLS_NONULLTERMINATE | NLS_SOURCEISLMBCS | NLS_TARGETISUNICODE
        , unicodeInfo
        ));

  // 変換後のQStringオブジェクト
  if (hasError())
    return QString();

  // UNICODEバイト列からQStringを作成して追加
  return QString::fromUtf16(
        reinterpret_cast<ushort*>(buffer.data())
        , static_cast<int>(retByteSize / UTF16_SIZE)
        );
}

Lmbcs Lmbcs::fromQString(const QString &from)
{
  // 変換する文字数が最大値を超えないようにする。
  int maxLen = std::min<int>(from.size(), static_cast<int>(UTF16_MAX_LEN));

  // 変換後のLMBCS文字列格納用バッファ
  CharArrayPtr buffer(new char[MAX_NLS_BUFSIZE]);
  WORD retByteSize = MAX_NLS_BUFSIZE;

  // QString(UTF16)からLMBCSに変換
  NLS_STATUS result = NLS_translate(
        reinterpret_cast<BYTE*>(const_cast<ushort*>(from.utf16()))
        , static_cast<WORD>(maxLen * UTF16_SIZE)
        , reinterpret_cast<BYTE*>(buffer.data())
        , &retByteSize
        , NLS_NONULLTERMINATE | NLS_SOURCEISUNICODE | NLS_TARGETISLMBCS
        , OSGetLMBCSCLS()
        );

  // 変換に失敗したら空のTextオブジェクトをエラー付きで返す。
  if (result != NLS_SUCCESS)
  {
    Lmbcs text;
    text.setLastResult(result);
    return text;
  }
  return Lmbcs(buffer.data(), retByteSize);
}

Lmbcs& Lmbcs::append(const Lmbcs& other)
{
  // QByteArray単位でつなげる。
  QByteArray x = value_ + other.value_;

  // int型で長さを取得する。
  int nLen = x.size();

  // 長さが制限値を超えていなければそのまま値にする。
  if (nLen <= static_cast<int>(MAX_NLS_BUFSIZE))
    value_ = x;

  // 長さが制限値を超えていたら
  else
  {
    // 制限値内で収まる文字数を取得する。
    WORD wCharLen = getCharSize(x.constData(), MAX_NLS_BUFSIZE);

    // 制限値内で収まるバイト数を取得する。
    WORD wByteLen = getByteSize(x.constData(), wCharLen);

    // 制限バイト数分だけvalue_に反映する。
    value_ = x.left(static_cast<int>(wByteLen));
  }

  // 自身への参照を返す。
  return *this;
}

LmbcsList Lmbcs::split(const Lmbcs &delimiter) const
{
  if (isEmpty())
    return LmbcsList();

  QString src = toQString();
  QString del = delimiter.toQString();
  QStringList list = src.split(del);
  return LmbcsList::fromQStringList(list);
}

WORD Lmbcs::getCharSize(const char* c_str, WORD byteSize, NLS_PINFO pInfo)
{
  WORD charSize = 0;
  NLS_string_chars(
        reinterpret_cast<const BYTE*>(c_str)
        , byteSize
        , &charSize
        , pInfo
        );
  return charSize;
}

WORD Lmbcs::getByteSize(const char* c_str, WORD len, NLS_PINFO pInfo)
{
  WORD size = 0;
  NLS_string_bytes(
        reinterpret_cast<const BYTE*>(c_str)
        , len
        , &size
        , pInfo
        );
  return size;
}

} // namespace CNExt
