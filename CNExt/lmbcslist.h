﻿#ifndef CNEXT_LMBCSLIST_H
#define CNEXT_LMBCSLIST_H

#include <CNExt/lmbcs.h>

#include <QStringList>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace CNExt {

template <typename T = DHANDLE>
class ObjectMemory
{
public:
  ObjectMemory()
    : handle_(NULLHANDLE)
    , ptr_(0)
  {
  }

  ObjectMemory(T handle, void *ptr = nullptr)
  : handle_(handle)
  , ptr_(ptr)
  {
  }

  virtual ~ObjectMemory()
  {
    this->free();
  }

  void assign(T handle, void *ptr = nullptr)
  {
    handle_ = handle;
    ptr_ = ptr;
  }

  const T &handle() const
  {
    unlock();
    return handle_;
  }

  const void *ptr() const
  {
    if (ptr_ == nullptr)
    {
      if (handle_ != NULLHANDLE)
        ptr_ = OSLockObject(handle_);
    }
    return ptr_;
  }

  void unlock() const
  {
    if (ptr_ != nullptr)
    {
      OSUnlockObject(handle_);
      ptr_ = nullptr;
    }
  }

  void free() const
  {
    unlock();
    if (handle_ != NULLHANDLE)
    {
      OSMemFree(handle_);
      handle_ = NULLHANDLE;
    }
  }

private:
  mutable T handle_;
  mutable void *ptr_;
};


class CNEXTSHARED_EXPORT LmbcsList
    : public Resultful
{
public:
  LmbcsList(bool fPrefixDataType = false);
  virtual ~LmbcsList();

  LmbcsList(const LmbcsList &other);
  LmbcsList &operator=(const LmbcsList &other);

  bool contains(const Lmbcs &entry) const;

  Lmbcs join(const Lmbcs &delimiter) const;

  WORD size() const;

  WORD numEntries() const;

  bool isEmpty() const;

  Lmbcs at(WORD index) const;

  Lmbcs first() const;

  Lmbcs last() const;

  WORD append(const Lmbcs &entry);

  void removeAt(WORD index);

  void removeFirst();

  void removeLast();

  bool removeOne(const Lmbcs &text);

  WORD removeAll(const Lmbcs &text);

  void clear();

  static LmbcsList fromQStringList(const QStringList &qlist);

  friend LmbcsList &operator<<(LmbcsList &list, const Lmbcs &entry);

private:
  bool assign(WORD numEntries);

private:
  bool fPrefixDataType_;
  ObjectMemory<DHANDLE> mem_;
};

inline Lmbcs LmbcsList::first() const
{
  return at(0);
}

inline Lmbcs LmbcsList::last() const
{
  return at(numEntries());
}

inline void LmbcsList::removeFirst()
{
  removeAt(0);
}

inline void LmbcsList::removeLast()
{
  removeAt(numEntries());
}

inline LmbcsList &operator<<(LmbcsList &list, const Lmbcs &entry)
{
  list.append(entry);
  return list;
}

} // namespace CNExt

#endif // CNEXT_LMBCSLIST_H
